package com.common.myroom

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 *
 * @ProjectName:    MyRoom
 * @Package:        com.common.myroom
 * @ClassName:      StudentDao
 * @Description:     类作用描述
 * @Author:         qb
 * @CreateDate:     2021/2/4 0004 下午 15:35
 * @UpdateUser:     更新者
 * @UpdateDate:     2021/2/4 0004 下午 15:35
 * @UpdateRemark:   更新说明
 * @Version:        1.0
 */
@Dao
interface StudentDao:BaseDao<Student> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(element:Student)

    @Query("select * from Student")
    fun getAllStudents():MutableList<Student>

    @Query("select * from Student where studentID = :studentID")
    fun getStudnet(studentID:Int):Student

    @Query("select * from Student order by studentID desc ")
    fun getAllByDateDesc():MutableList<Student>

    @Query("delete from Student")
    fun deleteAll()

}