package com.common.myroom

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *
 * @ProjectName:    MyRoom
 * @Package:        com.common.myroom
 * @ClassName:      Student
 * @Description:     类作用描述
 * @Author:         qb
 * @CreateDate:     2021/2/4 0004 下午 15:33
 * @UpdateUser:     更新者
 * @UpdateDate:     2021/2/4 0004 下午 15:33
 * @UpdateRemark:   更新说明
 * @Version:        1.0
 */
@Entity(tableName = "Student")
data class Student(
    @PrimaryKey(autoGenerate = true)
    var studentID: Int?,
    @ColumnInfo(name = "s_name")
    var studentName: String?,
    @ColumnInfo(name = "s_type")
    var studentType: String?
)