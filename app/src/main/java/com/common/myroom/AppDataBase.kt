package com.common.myroom

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 *
 * @ProjectName:    MyRoom
 * @Package:        com.common.myroom
 * @ClassName:      AppDataBase
 * @Description:     类作用描述
 * @Author:         qb
 * @CreateDate:     2021/2/4 0004 下午 15:35
 * @UpdateUser:     更新者
 * @UpdateDate:     2021/2/4 0004 下午 15:35
 * @UpdateRemark:   更新说明
 * @Version:        1.0
 */
@Database(entities = [Student::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun getStudentDao(): StudentDao
    companion object {
        val instance = Single.sin
    }
    private object Single {
        //这里创建的是数据库 User 是对应数据库名称，其他所有 例如Student Teacher 都是表
        val sin :AppDataBase= Room.databaseBuilder(
            MyApplication.instance(),
            AppDataBase::class.java,
            "User.db"
        )
            .allowMainThreadQueries()
            .build()
    }
}