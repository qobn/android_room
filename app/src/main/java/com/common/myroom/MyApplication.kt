package com.common.myroom

import android.app.Application
import kotlin.properties.Delegates

/**
 *
 * @ProjectName:    MyRoom
 * @Package:        com.common.myroom
 * @ClassName:      MyApplication
 * @Description:     类作用描述
 * @Author:         qb
 * @CreateDate:     2021/2/4 0004 下午 15:32
 * @UpdateUser:     更新者
 * @UpdateDate:     2021/2/4 0004 下午 15:32
 * @UpdateRemark:   更新说明
 * @Version:        1.0
 */
class MyApplication : Application() {

    companion object {

        var instance: MyApplication by Delegates.notNull()

        fun instance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}