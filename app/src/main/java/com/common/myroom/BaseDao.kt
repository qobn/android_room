package com.common.myroom

import androidx.room.*

/**
 *
 * @ProjectName:    MyRoom
 * @Package:        com.common.myroom
 * @ClassName:      BaseDao
 * @Description:     类作用描述
 * @Author:         qb
 * @CreateDate:     2021/2/4 0004 下午 15:34
 * @UpdateUser:     更新者
 * @UpdateDate:     2021/2/4 0004 下午 15:34
 * @UpdateRemark:   更新说明
 * @Version:        1.0
 */
@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: MutableList<T>)

    @Delete
    fun delete(element: T)

    @Delete
    fun deleteList(elements:MutableList<T>)

    @Delete
    fun deleteSome(vararg elements:T)

    @Update
    fun update(element: T)

}