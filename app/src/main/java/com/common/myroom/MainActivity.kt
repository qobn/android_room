package com.common.myroom

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sDao: StudentDao = AppDataBase.instance.getStudentDao()
        var s_1 = Student(1, "s1", "小学")
        var s_2 = Student(2, "s2", "小学")
        var s_3 = Student(3, "s3", "小学")
        var s_6 = Student(6, "s6", "大学")
        var s_5 = Student(5, "s5", "大学")
        var s_4 = Student(4, "s4", "大学")

        var sList: MutableList<Student> = mutableListOf<Student>()
        sList.add(s_1)
        sList.add(s_2)
        sList.add(s_3)
        sList.add(s_6)
        sList.add(s_5)
        sList.add(s_4)

        text_room.setOnClickListener {
            Log.e("TAG","点击了")
            Toast.makeText(this@MainActivity,"点击了",Toast.LENGTH_LONG).show()
            sDao.insertAll(sList)

            try {
                var sList_select_1: MutableList<Student> = sDao.getAllStudents()
                for (i in sList_select_1.indices) {
                    println(sList_select_1.get(i))
                }
            } catch (e: Exception) {
            }
        }
    }
}