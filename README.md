# android_room

#### 介绍
android kotlin  room 数据库简单操作

#### 软件架构
build 添加room依赖

#### 使用说明

1. 初始化创建 MyApplication
2. 创建对象Student增加room各类注释例如自增id,列名
3. 创建 interface StudentDao 增删查改方法处理
4. 创建 AppDataBase表管理类 执行内含创建库方法
5. 具体使用参考MainActivity
```
        val sDao: StudentDao = AppDataBase.instance.getStudentDao()
        var s_1 = Student(1, "s1", "小学")
        var s_2 = Student(2, "s2", "小学")
        var s_3 = Student(3, "s3", "小学")
        var s_6 = Student(6, "s6", "大学")
        var s_5 = Student(5, "s5", "大学")
        var s_4 = Student(4, "s4", "大学")

        var sList: MutableList<Student> = mutableListOf<Student>()
        sList.add(s_1)
        sList.add(s_2)
        sList.add(s_3)
        sList.add(s_6)
        sList.add(s_5)
        sList.add(s_4)

        text_room.setOnClickListener {
            Log.e("TAG","点击了")
            Toast.makeText(this@MainActivity,"点击了",Toast.LENGTH_LONG).show()
            sDao.insertAll(sList)

            try {
                var sList_select_1: MutableList<Student> = sDao.getAllStudents()
                for (i in sList_select_1.indices) {
                    println(sList_select_1.get(i))
                }
            } catch (e: Exception) {
            }
        }
```

#### 特技

- [入行程序员,不知道如何入手请点](http://www.zhengduimen.com)
